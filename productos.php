<?php
?>
<html lang="en">
<head>
  <title>Productos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Running Time</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Lista de productos</a></li>
        <li><a href="productos.php">Carga de Stop</a></li>
      <li><a href="venta.php">Venta</a></li>
      <li><a href="registroventa.php">Registro de Ventas</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
       
        <div class="container">
            <div class="col-sm-12">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Carga de Mercaderia</h1>
                    <b><hr></b>
                    <form action="carga.php" method="POST" name="frm">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Producto</label>
                                <input type="text" class="form-control text-danger" name="producto" id="producto" placeholder="Producto" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Talla</label>
                                <input type="text" class="form-control text-danger" name="talla"  id="talla" placeholder="Talla" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Marca</label>
                                <input type="text" class="form-control text-danger" name="marca" id="marca" placeholder="marca" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Proveedor</label>
                                <input type="text" class="form-control text-danger" name="provee" id="provee" placeholder="Proveedor" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Cantidad</label>
                                <input type="number" class="form-control text-danger" name="cant" id="cant" placeholder="Cantidad" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Precio</label>
                                <input type="number" class="form-control text-danger" name="precio" id="precio" placeholder="Precio" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                        <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Codigo</label>
                                <input type="text" class="form-control text-danger" name="cod"  id="cod" placeholder="Codigo" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-4">
                                <label for="name" class="h4 text-center">Num Caja</label>
                                <input type="text" class="form-control text-danger" name="caja" id="caja" placeholder="Numero de caja" required >
                                    <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                
                                    <div class="help-block with-errors"></div>
                            </div>
                                    
                                    <div class="form-group col-sm-4">
                                        <button type="submit" id="enviar" class="btn btn-success btn-lg pull-center ">Grabar</button>
                                    </div>
                            </div>
                                   
                            </form>
                        </div>
                    </div>
                </div>
     
        
        
        
        
 
        
    </div>
 </body>

</html>
