<?php
require_once ('conexion.php');
$con= conectarBD();


function getprod() {
    $con = conectarBD();
   
    $query = "SELECT * from producto ";
    $resut = mysqli_query($con, $query);
    $resulta = array();
    while ($data = mysqli_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}
function getRecibo(){
    $con= conectarBD();
    
    $query="SELECT COUNT(num_recibo) as total from movimiento";
    $resut = mysqli_query($con, $query);
    $result = mysqli_fetch_object($resut);
    
   return $result->total;
       
}
$data= getprod();
$ress=1+ $result['num_recibo']=getrecibo();
?>

<html lang="en">
<head>
  <title>Recibo</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Running Time</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Lista de productos</a></li>
        <li><a href="productos.php">Carga de Stop</a></li>
        <li><a href="venta.php">Venta</a></li>
      <li><a href="registroventa.php">Registro de Ventas</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
       
        <div class="container">
            
            <div class="col-sm-5">
                <div class="well" style="margin-top: 15px;">
                    <h1 class="text-center">Running Time</h1>
                    <center> <p>Ruc:80065703-9</p></center>
<!--                    <center><p>Fecha:<?php echo date("d/m/Y"); ?></p></center>-->
                    <b><hr></b>
                    <form action="cargaventa.php" method="POST" name="frm">
                        <div class="row">
                          
                             <div class="form-group col-sm-6">
                                <label for="name" class="h6 text-center">Fecha:</label>
                                <input type="text" class="form-control text-danger" name="fecha" id="fecha" value="<?php echo date("d/m/Y"); ?>"  >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="name" class="h6 text-center">Recibo</label>
                                <input type="text" class="form-control text-danger" name="recibo" id="recibo"  value="<?php echo $ress;?>" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="name" class="h6 text-center">Cantidad</label>
                                <input type="number" class="form-control text-danger" name="cant" id="cant" value="1" max="1000" min="1"  required >
                                <div class="help-block with-errors"></div>
                            </div>
                             
                             <div class="form-group col-sm-8">
                                <label for="name" class="h6 text-center">Producto</label>
                                <select class="form-control" id="prod"  name="prod">
                                    <?php foreach ($data as $d) : ?>
                                        <option value="<?php echo $d->producto.'&'.$d->id_inven.'&'.$d->precio; ?>"> <?php echo $d->producto;?>. <?php echo $d->talla;?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            
                            <div class="form-group col-sm-6">
                                <label for="name" class="h6 text-center">precio</label>
                                <input type="text" class="form-control text-danger" name="precio" id="precioecibo" >
                                <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group col-sm-6">
                                <label for="name" class="h6 text-center">Total:</label>
                                <input type="text" class="form-control text-danger" name="total" id="total"  value="" >
                                
                                <div class="help-block with-errors"></div>
                            </div>
                                   <div class="form-group col-sm-12">
                                       <button type="submit" id="imprimir" class="btn btn-success btn-lg pull-center "  >Guardar</button>
                                    </div>     
                        </div>
                    </div>
                </div>
    </div>
 </body>
 <script src="js/script.js"></script>

</html>
