<?php require_once ('conexion.php');
 $con= conectarBD();
 
 
 function getventa() {
    $con = conectarBD();

    $query = 'select * from movimiento ';
 

    $resut = mysqli_query($con, $query);
    $resulta = array();
    while ($data = mysqli_fetch_object($resut)) {
        $resulta[] = $data;
    }

    return $resulta;
}
$data= getventa();
?>
<html lang="en">
<head>
  <title>Registro Venta</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="assets/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Running Time</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Lista de productos</a></li>
        <li><a href="productos.php">Carga de Stop</a></li>
      <li><a href="venta.php">Venta</a></li>
      <li><a href="#">Registro de Ventas</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
    </ul>
  </div>
</nav>
    <div class="container">
        <h3>Productos Vendidos</h3>
        
      <table class="table table-bordered">
    <thead>
      <tr>
        <th>Cantidad</th>
        <th>Producto</th>
        <th>Monto</th>
        <th>Numero Recibo</th>
        <th>Fecha Venta</th>
      </tr>
    </thead>
    <tbody>
    
        <?php if ($data != ''):
     foreach ($data as $d):?>
            <tr>
        <td><?php echo $d->cant; ?></td>
        <td><?php echo $d->descripcion; ?></td>
        <td><?php echo $d->monto; ?></td>
        <td> <?php echo $d->num_recibo; ?></td>
        <td><?php echo $d->fecha;?></td>
        
      </tr>
      <?php endforeach;?>
        
          <?php else :  ?>
        <tr>
        <td><?php  ?></td>
        <td><?php  ?></td>
        <td> <?php  ?></td>
        <td><?php ?></td>
        <td><?php ?></td>
      </tr>
       <?php endif;  ?>
    </tbody>
  </table>  
        
        
        
        
 
        
    </div>
 </body>

</html>