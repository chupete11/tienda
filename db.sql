/*
Navicat MySQL Data Transfer

Source Server         : conect
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : fernando

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-02-19 17:40:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for movimiento
-- ----------------------------
DROP TABLE IF EXISTS `movimiento`;
CREATE TABLE `movimiento` (
  `id_movi` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cant` int(11) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `num_recibo` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id_movi`),
  UNIQUE KEY `id_movi` (`id_movi`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of movimiento
-- ----------------------------
INSERT INTO `movimiento` VALUES ('40', '10', 'Remera Axn2016', '50000', '1', '2019-02-17');
INSERT INTO `movimiento` VALUES ('41', '5', 'Remera Axn2015', '35000', '2', '2019-02-18');
INSERT INTO `movimiento` VALUES ('42', '10', 'Remera Axn2015', '35000', '3', '2019-02-18');
INSERT INTO `movimiento` VALUES ('43', '5', 'Remera Axn2015', '35000', '4', '2019-02-18');
INSERT INTO `movimiento` VALUES ('44', '5', 'Remera Axn2015', '35000', '5', '2019-02-18');

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id_inven` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto` varchar(255) NOT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `proveedor` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `num_caja` varchar(255) NOT NULL,
  `talla` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_inven`),
  UNIQUE KEY `id_inven` (`id_inven`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES ('22', 'Remera Axn2016', 'PUMA', 'PUMA', '40', '55000', '55525', '7', 'MM');
INSERT INTO `producto` VALUES ('30', 'Remera Axn2016', 'PUMA', 'PUMA', '50', '35000', '55524', '8', 'MP');
INSERT INTO `producto` VALUES ('31', 'Remera Axn2016', 'PUMA', 'PUMA', '50', '35000', '55523', '8', 'MG');
INSERT INTO `producto` VALUES ('39', 'Remera AXN2016', 'PUMA', 'Puma', '30', '50000', '55555555', '8', 'MXL');
INSERT INTO `producto` VALUES ('38', 'Remera AXN2016', 'PUMA', 'Puma', '45', '35000', '55555555', '8', 'MXL');

-- ----------------------------
-- Table structure for proveedor
-- ----------------------------
DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor` (
  `id_provee` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `marca` varchar(255) DEFAULT NULL,
  `proveedor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_provee`),
  UNIQUE KEY `id_provee` (`id_provee`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proveedor
-- ----------------------------
INSERT INTO `proveedor` VALUES ('1', 'Juan Ramon', 'juanr@gmail.com', '021555555', 'greed', 'Tramontina');

-- ----------------------------
-- Table structure for recibo
-- ----------------------------
DROP TABLE IF EXISTS `recibo`;
CREATE TABLE `recibo` (
  `id_recibo` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_recibo`),
  UNIQUE KEY `id_recibo` (`id_recibo`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recibo
-- ----------------------------
INSERT INTO `recibo` VALUES ('1');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id_usua` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nivel` varchar(100) NOT NULL,
  PRIMARY KEY (`id_usua`),
  UNIQUE KEY `id_usua` (`id_usua`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Fernando', 'fernandoarielm8@gmail.com', '123', 'administrador');

-- ----------------------------
-- Table structure for venta
-- ----------------------------
DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta` (
  `id_vent` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `Obs` varchar(255) NOT NULL,
  PRIMARY KEY (`id_vent`),
  UNIQUE KEY `id_vent` (`id_vent`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of venta
-- ----------------------------
INSERT INTO `venta` VALUES ('1', 'destornillador', '1', '1500', '1', '-');
DROP TRIGGER IF EXISTS `restarStock`;
DELIMITER ;;
CREATE TRIGGER `restarStock` AFTER INSERT ON `movimiento` FOR EACH ROW BEGIN
Update `PRODUCTO` set `cantidad` = `cantidad` - `New.canti`
where `producto` = `descripcion`;
END
;;
DELIMITER ;
